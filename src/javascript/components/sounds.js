export function hitSound() {
  const sounds = document.getElementById('hit-sound');
  sounds.play();
}

export function criticalHitSound() {
  const sounds = document.getElementById('critical-hit-sound');
  sounds.play();
}

export function victorySound() {
  const sounds = document.getElementById('victory-sound');
  sounds.play();
}