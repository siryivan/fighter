import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';

export function renderArena(selectedFighters) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  const [playerOne, playerTwo] = selectedFighters;
  fight(playerOne, playerTwo).then(fighter => showWinnerModal(fighter));
}

function createArena(selectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);
  const shields = createContainerShields(...selectedFighters);
  const hits = createContainerHits(...selectedFighters);
  const criticals = createContainerCriticals(...selectedFighters);
  const criticalindicators = createCriticalIndicators(...selectedFighters);

  arena.append(healthIndicators, fighters, shields, hits, criticals, criticalindicators );
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter, secondFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}

function createContainerShields(firstFighter, secondFighter) {
  const container = createElement({ tagName: 'div', className: `arena___shields-container` });
  const firstFighterShield = createShield(firstFighter, 'left');
  const secondFighterShield = createShield(secondFighter, 'right');

  container.append(firstFighterShield, secondFighterShield);
  return container;
}

function createShield(fighter, position) {
  const imgElement = createShieldImage();
  const positionClassName = position === 'right' ? 'arena___right-shield' : 'arena___left-shield';
  const shieldElement = createElement({
    tagName: 'div',
    className: `${positionClassName}`,
    attributes: { id: `${position}-shield` }
  });

  shieldElement.append(imgElement);
  return shieldElement;
}

function createShieldImage() {
  const attributes = {
    src: './resources/images/shield.png',
    alt: 'shield'
  };
  return createElement({
    tagName: 'img',
    className: 'shield-img',
    attributes,
  });
}

function createContainerHits(firstFighter, secondFighter) {
  const container = createElement({ tagName: 'div', className: `arena___hit-container` });
  const firstFighterHit = createHit(firstFighter, 'left');
  const secondFighterHit = createHit(secondFighter, 'right');

  container.append(firstFighterHit, secondFighterHit);
  return container;
}

function createHit(fighter, position) {
  const imgElement = createHitImage();
  const positionClassName = position === 'right' ? 'arena___right-hit' : 'arena___left-hit';
  const hitElement = createElement({
    tagName: 'div',
    className: `${positionClassName}`,
    attributes: { id: `${position}-hit` }
  });

  hitElement.append(imgElement);
  return hitElement;
}

function createHitImage() {
  const attributes = {
    src: './resources/images/hit.png',
    alt: 'hit'
  };
  return createElement({
    tagName: 'img',
    className: 'hit-img',
    attributes,
  });
}

function createContainerCriticals(firstFighter, secondFighter) {
  const container = createElement({ tagName: 'div', className: `arena___criticals-container` });
  const firstFighterCritical = createCritical(firstFighter, 'left');
  const secondFighterCritical = createCritical(secondFighter, 'right');

  container.append(firstFighterCritical, secondFighterCritical);
  return container;
}

function createCritical(fighter, position) {
  const imgElement = createCriticalImage();
  const positionClassName = position === 'right' ? 'arena___right-critical' : 'arena___left-critical';
  const criticalElement = createElement({
    tagName: 'div',
    className: `${positionClassName}`,
    attributes: { id: `${position}-critical` }
  });

  criticalElement.append(imgElement);
  return criticalElement;
}

function createCriticalImage() {
  const attributes = {
    src: './resources/images/critical.png',
    alt: 'critical'
  };
  return createElement({
    tagName: 'img',
    className: 'critical-img',
    attributes,
  });
}

function createCriticalIndicators(firstFighter, secondFighter) {
  const container = createElement({ tagName: 'div', className: `arena___criticalindicators-container` });
  const firstFighterCritical = createCriticalIndicator(firstFighter, 'left');
  const secondFighterCritical = createCriticalIndicator(secondFighter, 'right');

  container.append(firstFighterCritical, secondFighterCritical);
  return container;
}

function createCriticalIndicator(fighter, position) {
  const imgElement = createCriticalImage();
  const positionClassName = position === 'right' ? 'arena___right-criticalindicator' : 'arena___left-criticalindicator';
  const criticalIndicatorElement = createElement({
    tagName: 'div',
    className: `${positionClassName}`,
    attributes: { id: `${position}-criticalindicator` }
  });

  criticalIndicatorElement.append(imgElement);
  return criticalIndicatorElement;
}