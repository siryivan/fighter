import { controls } from '../../constants/controls';
import Fighter from './fighter';
import { criticalHitSound, hitSound, victorySound } from './sounds';
import { changeHealthbarWidth, showAttack, toggleShield, toggleCriticalIndicator } from './fightersView';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let winnerPlayer = '';
    const activeButton = new Set();
    const fighters = createFighters(firstFighter, secondFighter);
    const onkeydown = (e) => {
      handleKeyDown(e, activeButton, fighters);
      winnerPlayer = endFight(fighters);
      if (winnerPlayer) {
        victorySound();
        (winnerPlayer === 1) && resolve(firstFighter);
        (winnerPlayer === 2) && resolve(secondFighter);
        document.removeEventListener('keydown', onkeydown);
        document.removeEventListener('keyup', onkeyup);
      }
    };
    const onkeyup = (e) => handleKeyUp(e, activeButton, fighters);
    document.addEventListener('keydown', onkeydown);
    document.addEventListener('keyup', onkeyup);
  });
}

export function getDamage(attacker, defender) {
  const damage = ((getHitPower(attacker) - getBlockPower(defender)) < 0) ? 0 : (getHitPower(attacker) - getBlockPower(defender));
  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}

function getCriticalHitPower(fighter) {
  return fighter.attack * 2;
}

function createFighters(firstFighter, secondFighter) {
  return {
    'firstFighter': new Fighter(1, firstFighter),
    'secondFighter': new Fighter(2, secondFighter)
  };
}

function handleKeyDown(e, btnSet, {firstFighter, secondFighter}) {
  if(btnSet.has(e.code)) return;
  btnSet.add(e.code);
  if(e.code === controls.PlayerOneBlock) {
    return toggleShield('left');
  }
  if(e.code === controls.PlayerTwoBlock) {
    return toggleShield('right');
  }
  fightAction(btnSet, {firstFighter, secondFighter});
}

function handleKeyUp(e, btnSet, {firstFighter, secondFighter}) {
  btnSet.delete(e.code);
  switch (e.code) {
    case controls.PlayerOneAttack:
      return firstFighter.canAttack = true;
    case controls.PlayerTwoAttack:
      return secondFighter.canAttack = true;
    case controls.PlayerOneBlock:
      return toggleShield('left');
    case controls.PlayerTwoBlock:
      return toggleShield('right');
  }
}

function fightAction(btnSet, {firstFighter, secondFighter}) {
  switch(true) {
    case doAttack(btnSet,
                  controls.PlayerOneAttack,
                  controls.PlayerOneBlock,
                  firstFighter.canAttack):
      return effectAttack(firstFighter, secondFighter, btnSet, controls.PlayerTwoBlock);

    case doAttack(btnSet,
                  controls.PlayerTwoAttack,
                  controls.PlayerTwoBlock,
                  secondFighter.canAttack,
                  hitSound):
      return effectAttack(secondFighter, firstFighter, btnSet, controls.PlayerOneBlock);

    case doCriticalAttack(btnSet, 
                          controls.PlayerOneCriticalHitCombination, 
                          controls.PlayerOneBlock, 
                          firstFighter.canCriticalAttack):
      return effectCriticalAttack(firstFighter, secondFighter);

    case doCriticalAttack(btnSet,
                          controls.PlayerTwoCriticalHitCombination,
                          controls.PlayerTwoBlock, 
                          secondFighter.canCriticalAttack):
      return effectCriticalAttack(secondFighter, firstFighter);

    default:
      return;
  }
}

function doAttack(btnSet, controlAttack, controlBlock, canAttack) {
  hitSound();
  return (btnSet.has(controlAttack) && !btnSet.has(controlBlock) && canAttack);
}


function effectAttack(attacker, defender, btnSet, controlDefense) {
  hitSound();
  const positionAttacker = (attacker.num === 1) ? 'left' : 'right';
  showAttack(positionAttacker, 'hit');
  attacker.canAttack = false;
  const damage = btnSet.has(controlDefense) ? getDamage(attacker, defender) : getHitPower(attacker);
  defender.decreaseHealth(damage);
  const positionDefender = (defender.num === 1) ? 'left' : 'right';
  changeHealthbarWidth(defender, positionDefender);
}

function doCriticalAttack(btnSet, controlAttack, controlBlock, canCriticalAttack) {
  return (checkCriticalAttack(btnSet, controlAttack) && !btnSet.has(controlBlock) && canCriticalAttack);
}

function checkCriticalAttack(btnSet, control) {
  if(btnSet.size < control) {
    return false;
  }
  return controlValuesInSet(btnSet, control);
}

function controlValuesInSet(set, control) {
  const len = control.length;
  let result = true;
  for(let i = 0; i < len; i++) {
    if(!set.has(control[i])) {
      result = false;
      break;
    }
  }
  return result;
}

function delayCriticalAttack(player, position) {
  player.canCriticalAttack = false;
  toggleCriticalIndicator(player.canCriticalAttack, position);
  setTimeout(() => {
    player.canCriticalAttack = true;
    toggleCriticalIndicator(player.canCriticalAttack, position);
  }, 10000);
}

function effectCriticalAttack(attacker, defender) {
  criticalHitSound();
  const positionAttacker = (attacker.num === 1) ? 'left' : 'right';
  const positionDefender = (defender.num === 1) ? 'left' : 'right';
  showAttack(positionAttacker, 'critical');
  defender.decreaseHealth(getCriticalHitPower(attacker));
  changeHealthbarWidth(defender, positionDefender);
  delayCriticalAttack(attacker, positionAttacker);
}


function endFight({firstFighter, secondFighter}) {
  switch (true) {
    case (secondFighter.health === 0):
      return 1;
    case (firstFighter.health === 0):
      return 2;  
    default:
      return '';
  }
}
