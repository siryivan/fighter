function Fighter(num, {health, defense, attack }) {
    this.num = num;
    this.health = health;
    this.initialHealth = health;
    this.defense = defense;
    this.attack = attack;
    this.canAttack = true;
    this.canCriticalAttack = true;
  }

  Fighter.prototype.decreaseHealth = function(value) {
    this.health = ((this.health - value) > 0) ? (this.health - value) : 0;
  };

export default Fighter;
